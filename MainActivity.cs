﻿using System;
using System.IO;
using System.Json;
using System.Net;
using System.Threading.Tasks;
using Android.App;
using Android.Widget;
using Android.OS;

namespace LeagueStatGrabber
{
    [Activity(Label = "LeagueStatGrabber", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get the latitude/longitude EditBox and button resources:
            EditText championText = FindViewById<EditText>(Resource.Id.playerNameText);
            Button button = FindViewById<Button>(Resource.Id.getDataButton);

            // When the user clicks the button ...
            button.Click += async (sender, e) => {

                // Get the latitude and longitude entered by the user and create a query.
                string playerName = championText.Text;

                // Fetch the weather information asynchronously, 
                // parse the results, then update the screen:
                await ApiCalls.GetData(playerName);
                ParseAndDisplay();
            };
        }
        
        private void ParseAndDisplay()
        {
           
        }
    }

}

