﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace LeagueStatGrabber
{
    class Player
    {
        public int ProfileIconId;
        public string Name;
        public int SummonerLevel;
        public int AccountId;
        public int PlayerId;
        private static Player instance;
        public List<ChampionData> championMastery;

        public static Player GetInstance
        {
            get
            {
                if(instance == null)
                    instance = new Player();
                return instance;
            }
        }

        public Player()
        {
            championMastery = new List<ChampionData>();
        }
        
    }
}