﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;


namespace LeagueStatGrabber
{
    static class ApiCalls
    {
        private static string APIKey = "RGAPI-61aed2ec-270f-4837-b5e3-e748218d2892";

        public static async Task<bool> GetData(string playerName)
        {
            await GetPlayerInfo(playerName);
            await GetChampionData();
            return true;
        }
        public static  async Task<JsonValue> GetPlayerInfo(string playerName)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://euw1.api.riotgames.com/lol/summoner/v3/summoners/by-name/"+playerName+"?api_key="+APIKey));
            request.ContentType = "application/json";
            request.Method = "GET";
            WebResponse response;
            JsonValue jsonDoc;
            // Send the request to the server and wait for the response:
            using (response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());
                    
                    // Return the JSON document:
                    
                    
                }
            }
            Player.GetInstance.AccountId = jsonDoc["accountId"];
            Player.GetInstance.PlayerId = jsonDoc["id"];
            Player.GetInstance.SummonerLevel = jsonDoc["summonerLevel"];
            Player.GetInstance.Name = jsonDoc["name"];
            Player.GetInstance.ProfileIconId = jsonDoc["profileIconId"];
            
            return jsonDoc;
        }

        private static async Task<bool> GetChampionData()
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://euw1.api.riotgames.com/lol/champion-mastery/v3/champion-masteries/by-summoner/" + Player.GetInstance.PlayerId + "?api_key=" + APIKey));
            request.ContentType = "application/json";
            request.Method = "GET";
            JsonValue jsonDoc;
            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());


                }
            }

            for (int i = 0; i < jsonDoc.Count; i++)
            {
                JsonValue championDoc = jsonDoc[i];
                ChampionData champion = new ChampionData();
                string tmp = championDoc.ToString();
                tmp.Remove(0, 1);
                tmp.Remove(tmp.Length - 1);
                championDoc = tmp;
                champion = JsonConvert.DeserializeObject<ChampionData>(championDoc);
                Player.GetInstance.championMastery.Add(champion);
            }
            return true;
        }
    }
}