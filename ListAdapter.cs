﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using Object = Java.Lang.Object;

namespace LeagueStatGrabber
{
    class ListAdapter : BaseExpandableListAdapter
    {
        private Context context;
        private List<string> listTitles;
        private Dictionary<string, List<string>> listDetails;

        public ListAdapter(Context context, List<string> listTitles, Dictionary<string, List<string>> listDetails)
        {
            this.context = context;
            this.listTitles = listTitles;
            this.listDetails = listDetails;

        }
        public override Object GetChild(int groupPosition, int childPosition)
        {
            return listDetails[listTitles[groupPosition]][childPosition];
        }

        public override long GetChildId(int groupPosition, int childPosition)
        {
            return childPosition;
        }

        public override int GetChildrenCount(int groupPosition)
        {
            return listDetails[listTitles[groupPosition]].Count;
        }

        public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView,
            ViewGroup parent)
        {
            string expandedListText = (string) GetChild(groupPosition, childPosition);
            if (convertView == null)
            {
                LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .GetSystemService(Context.LayoutInflaterService);
                convertView = layoutInflater.Inflate(Resource.Layout.list_item,null);
            }
            TextView expandedListTextView = (TextView)convertView
                .FindViewById(Resource.Id.expandedListItem);
            expandedListTextView.SetText(expandedListText,null);
            return convertView;
        }

        public override Object GetGroup(int groupPosition)
        {
            return listTitles[groupPosition];
        }

        public override long GetGroupId(int groupPosition)
        {
            return groupPosition;
        }

        public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
        {
            string listTitle = (string)GetGroup(groupPosition);
            if (convertView == null)
            {
                LayoutInflater layoutInflater = (LayoutInflater)this.context.
                    GetSystemService(Context.LayoutInflaterService);
                convertView = layoutInflater.Inflate(Resource.Layout.list_group, null);
            }
            TextView listTitleTextView = (TextView)convertView
                .FindViewById(Resource.Id.listTitle);
            listTitleTextView.SetTypeface(Typeface.Default, TypefaceStyle.Bold);
            listTitleTextView.SetText(listTitle,null);
            return convertView;
        }

        public override bool IsChildSelectable(int groupPosition, int childPosition)
        {
            return true;
        }

        public override int GroupCount { get; }
        public override bool HasStableIds { get; }
    }
}